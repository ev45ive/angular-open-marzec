import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  // inputs: ['playlists:items']
  // outputs:['selectedChange']
})
export class PlaylistListComponent implements OnInit {

  @Input('items') playlists: Playlist[] | null = [];

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>()

  @Input()
  selectedId?: Playlist['id'] 

  select(playlist_id: string) {    
    this.selectedIdChange.emit(playlist_id)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
