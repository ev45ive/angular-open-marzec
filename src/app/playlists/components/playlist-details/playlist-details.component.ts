import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() 
  playlist?: Playlist

  @Output() delete = new EventEmitter<Playlist['id']>();
  @Output() edit = new EventEmitter();

  clickEdit(){
    this.edit.emit()
  }

  clickDelete(){
    this.delete.emit(this.playlist!.id)
  }

  constructor() { }

  ngOnInit(): void {
    // if(!this.playlist){ throw new Error('Playlist is required')}
  }


}
