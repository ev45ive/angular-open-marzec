import { AfterViewInit, Component, DoCheck, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, OnChanges, OnDestroy, DoCheck, AfterViewInit {

  @Input() playlist!: Playlist
  // draft!: Playlist

  showDetails = true

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  @ViewChild('formRef', { read: NgForm, static: true })
  formRef?: NgForm

  constructor() {
    console.log('constructor')
  }

  ngAfterViewInit(): void {
    // setTimeout(() => {
      // this.formRef?.setValue({
      //   name: this.playlist.name,
      //   details: {
      //     public: this.playlist.public,
      //     description: this.playlist.description,
      //   }
      // })
      // debugger; //2
    // }, 0)
    // debugger; //1
  }

  ngDoCheck(): void {
    console.log('ngDoCheck')
  }

  cancelClick() { this.cancel.emit() }

  formSubmit(formRef: NgForm) {
    this.save.emit({
      ...this.playlist,
      name: formRef.value.name,
      ...formRef.value.details
    })
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy')
  }

  ngOnChanges(changes: SimpleChanges): void {
    // debugger
    console.log('ngOnChanges', changes)
    // this.draft = { ...this.playlist }
  }


  ngOnInit(): void {
    console.log('ngOnInit')

    // this.draft = Object.assign({},this.playlist)
  }


}
