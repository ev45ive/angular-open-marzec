import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuardService } from '../core/guards/logged-in-guard/logged-in-guard.service';
import { PlaylistViewComponent } from './containers/playlist-view/playlist-view.component';

const routes: Routes = [
  {
    path:'playlists',
    canActivate:[LoggedInGuardService],
    children:[
      {
        path: '',
        component: PlaylistViewComponent
      }, 
      {
        path: ':playlist_id',
        component: PlaylistViewComponent
      }
    ]
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
