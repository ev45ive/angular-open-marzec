import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';

import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { PlaylistListItemComponent } from './components/playlist-list-item.component';
import { PlaylistViewComponent } from './containers/playlist-view/playlist-view.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    PlaylistDetailsComponent,
    PlaylistListComponent,
    PlaylistFormComponent,
    PlaylistListItemComponent,
    PlaylistViewComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  // exports: [PlaylistViewComponent]
})
export class PlaylistsModule { }
