import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToggleContainerDirective } from './toggle-container.directive';
import { ToggleTriggerDirective } from './toggle-trigger.directive';
import { ToggleCollapsibleDirective } from './toggle-collapsible.directive';



@NgModule({
  declarations: [ToggleContainerDirective, ToggleTriggerDirective, ToggleCollapsibleDirective],
  imports: [
    CommonModule
  ],
  exports: [ToggleContainerDirective, ToggleTriggerDirective, ToggleCollapsibleDirective]
})
export class ToggleModule { }
