import { Directive, ElementRef, HostBinding, HostListener, Input, Optional } from '@angular/core';
import { ToggleContainerDirective } from './toggle-container.directive';
import { ToggleTriggerDirective } from './toggle-trigger.directive';

@Directive({
  selector: '[appToggleCollapsible]',
  // host:{
  //   '[class.show]':'isOpen'
  // }
})
export class ToggleCollapsibleDirective {

  @Input() appToggleCollapsible?: ToggleTriggerDirective | string = ''

  @Input() noAutoClose = false;

  @HostBinding('attr.aria-expanded')
  @HostBinding('class.show')
  isOpen = false


  @HostListener('document:click', ['$event'])
  clickedAnythere(event: any) {
    if (this.noAutoClose) { return }

    if (!this.appToggleCollapsible || 'string' === typeof this.appToggleCollapsible) { return }

    if (this.isOpen && !event.path.includes(this.elem.nativeElement)
      && !event.path.includes(this.appToggleCollapsible.elem.nativeElement)) {
      this.isOpen = false
    }
  }

  constructor(
    private elem: ElementRef<HTMLElement>,
    @Optional() private parent: ToggleContainerDirective,
  ) {
    // console.log('hello', this.elem)
  }

  ngOnInit(): void {
    if (!this.appToggleCollapsible && this.parent) {
      this.appToggleCollapsible = this.parent.appToggleTriggerDirective
    }
    if (!this.appToggleCollapsible || 'string' === typeof this.appToggleCollapsible) { return }


    this.appToggleCollapsible.toggled.subscribe(() => {
      // this.elem.nativeElement.classList.toggle('show')
      this.isOpen = !this.isOpen
    })

  }
}
