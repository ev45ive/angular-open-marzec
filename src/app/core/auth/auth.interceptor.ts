import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { from, iif, Observable, of, range, throwError, timer, zip } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { catchError, filter, map, mergeAll, mergeMap, retryWhen } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    

    if (request.url.includes('skip_my_interceptor')) {
      return next.handle(request)
    }

    const authRequest = this.buildRequest(request)

    return next.handle(authRequest).pipe(
      retryWhen(errors => { // next to retry, error or complete to giveup

        const retries = 5;
        const connectionErrors = errors.pipe(
          mergeMap(error => iif(
            () => error instanceof HttpErrorResponse && error.status === 0,
            of('retry!'),
            throwError(error)
          ))
        )
        // from([1,2,3])
        return zip(range(1, retries), connectionErrors).pipe(
          mergeMap(
            ([i, error]) => iif(() => i < retries,
              timer(i * i * 500),
              throwError(error))
          )
        )
      }),
      catchError((error) => {
        if (!(error instanceof HttpErrorResponse)) {
          console.error(error)
          return throwError(new Error('Unexpected error!'))
        }
        if (error.status === 401) {

          return from(this.auth.login()).pipe(
            mergeMap(() => {
              return next.handle(this.buildRequest(request))
            })
          )
        }

        return throwError(new Error(error.error.error.message))
      })
    )
  }

  private buildRequest(request: HttpRequest<unknown>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      },
    });
  }
}

// const obs = from([
//   1,2,3
// ]).pipe(
//   mergeMap(n => from([ n * 1,n * 2,n * 3,])),
//   // mergeAll()
// )



// A.next = B
// B.next = C
// C.next = HttpBackend


// http.get(x) -> A.handle(req)