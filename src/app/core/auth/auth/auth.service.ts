import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { BehaviorSubject, iif, of } from 'rxjs';
import { filter, startWith, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserProfile } from '../../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user = new BehaviorSubject<UserProfile | null>(null)
  readonly userChange = this.user.asObservable()

  getUser() {
    return iif(() => !!this.user.getValue(),
      of(this.user.getValue()),
      this.userChange.pipe(filter(user => !!user), take(1))
    )
  }

  constructor(
    private http: HttpClient,
    private oauth: OAuthService) {
    oauth.configure(environment.authTokenFlowConfig)
    // oauth.setupAutomaticSilentRefresh()
  }

  getToken() { return this.oauth.getAccessToken() }

  async init() {
    await this.oauth.tryLoginImplicitFlow()

    // console.log(this.oauth.getAccessToken())

    if (!this.oauth.getAccessToken()) {
      // await this.oauth.initLoginFlow()
      // await this.login()
    } else {
      this.fetchUserProfile().subscribe(user => this.user.next(user))
    }
  }

  fetchUserProfile() {
    return this.http.get<UserProfile>('https://api.spotify.com/v1/me')
  }

  async login() {
    // this.oauth.initLoginFlow()
    await this.oauth.initLoginFlowInPopup({
      height: 600, width: 500
    })

    this.fetchUserProfile().subscribe(user => this.user.next(user))
  }

  async logout() {
    this.oauth.logOut()
    this.user.next(null)
  }

}
