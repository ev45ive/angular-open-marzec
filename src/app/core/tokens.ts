import { InjectionToken } from "@angular/core";
import { Album } from "./model/search";

export const SEARCH_API_URL = new InjectionToken<string>('SEARCH_API_URL')
export const INITIAL_SEARCH_RESULTS = new InjectionToken<Album[]>('INITIAL_SEARCH_RESULTS')