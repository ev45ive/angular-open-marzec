export interface Entity {
  id: string;
  name: string;
}
export interface Track extends Entity {
}

export interface Playlist extends Entity {
  public: boolean;
  description: string;
  tracks?: Track[]
}

// const p: Playlist = {
//   id: '123',
//   name: 'MyPlaylist',
//   public: true,
//   description: 'awesome',
//   tracks: [
//     {
//       id: '123', name: 'track123'
//     }
//   ]
// }

// if (p.tracks) p.tracks.length;
// const l1 = p.tracks ? p.tracks.length : 0
// const l2 = p.tracks && p.tracks.length
// const l3 = p.tracks?.length
// const l4 = p.tracks ? p.tracks : []
// const l5 = p.tracks ?? []
// const l6 = (p.tracks ?? []).length

// const id: number| string = 123
// if('string'=== typeof id){
//   p.id.length
// }else{
//   p.id.toExponential()
// }

// export class Playlist implements Playlist {
//   constructor(){}
//   getId(){}
//   setId(){}
// }
