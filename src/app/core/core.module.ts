import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { AuthModule } from './auth/auth.module';
import { INITIAL_SEARCH_RESULTS, SEARCH_API_URL } from './tokens';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthModule,
    HttpClientModule,
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'',headerName:''
    // })
    HttpClientXsrfModule.disable()
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass MySuperExtraAwesomeAuthHttpClient
    // }
    {
      provide: INITIAL_SEARCH_RESULTS,
      useValue:[]
    },
    {
      provide: SEARCH_API_URL,
      useValue: environment.api_url
    },
  ],
})
export class CoreModule { }
