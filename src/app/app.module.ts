import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistDetailsComponent } from './playlists/components/playlist-details/playlist-details.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { SandboxModule } from './sandbox/sandbox.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // Vendors / Libraries
    BrowserModule,
    SharedModule,
    // Core
    CoreModule,
    // Features
    PlaylistsModule,
    // MusicSearchModule,
    // Overrides 
    SandboxModule,
    // Main routing
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent/* ,HeaderComponent,SidebarComponent */]
})
export class AppModule /* implements DoBootstrap */ {

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   appRef.bootstrap(AppComponent,'app-root')
  //    getConfigFromServer().then( config => 
  //   appRef.bootstrap(AppComponent,'lubie-placki')
  // }
}
