import { Inject, NgModule } from '@angular/core';
import { RouterModule, Routes, ROUTES } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'search',
    loadChildren: () => {
      return import('./music-search/music-search.module')
        .then(m => m.MusicSearchModule)
    }
  },
  {
    path: '**',
    // component: PageNotFoundComponent,
    redirectTo: 'search',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // enableTracing: true, // log to console
    // useHash: true,

  })],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor(@Inject(ROUTES) routes: any) {
    // console.log(routes)
  }
}
