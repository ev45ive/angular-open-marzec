import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { NavigationError, NavigationStart, Router } from '@angular/router';
import { AuthService } from './core/auth/auth/auth.service';

@Component({
  selector: 'app-root, lubie-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'Szkolenie Angular';
  message = ''
  user = { name: 'Guest' };

  time = ''

  constructor(
    private router: Router,
    public auth: AuthService,
    private cdr: ChangeDetectorRef) {
    // cdr.detach()

    router.events.subscribe(event => {
      console.log(event)
      if (event instanceof NavigationError) {
        this.message = event.error
      }
      if (event instanceof NavigationStart) {
        this.message =''
      }
    })
    // setInterval(() => {
    //   // cdr.detectChanges()
    //   this.time = new Date().toLocaleTimeString()
    // }, 1000)
  }

  login() {
    // this.user = { name: 'Admin' }
    this.auth.login()

    // this.cdr.detectChanges()
  }

  logout() {
    this.auth.logout()
  }

  getTime() {
    console.log('getTIme')
    // return new Date().toLocaleTimeString()
    // return Math.random() // ExpressionChangedAfterItHasBeenCheckedError
    // https://angular.io/errors/NG0100
    return this.time
  }
}
