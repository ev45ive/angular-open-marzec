import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { environment } from 'src/environments/environment';
import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { AlbumsGridComponent } from './components/albums-grid/albums-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { INITIAL_SEARCH_RESULTS, SEARCH_API_URL } from '../core/tokens';
import { MusicSearchService } from '../core/services/music-search/music-search.service';
import { SharedModule } from '../shared/shared.module';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
// import { MusicSearchService } from '../core/services/music-search/music-search.service';


@NgModule({
  declarations: [
    AlbumSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    AlbumDetailsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule
  ],
  providers: [
    
    // {
    //   provide: MusicSearchService,
    //   useFactory(api_url: string) {
    //     return new MusicSearchService(api_url)
    //   },
    //   deps: [SEARCH_API_URL]
    // },
    // {
    //  provide:  AbstractMusicSearchService,
    //  useClass: SpotifyMusicSearchService,
    // //  deps: [SEARCH_API_URL]
    // }
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    // },
    // MusicSearchService,
    // {
    //   provide: SearchService,
    //   useExisting: MusicSearchService
    // }
  ]
})
export class MusicSearchModule { }
