import { NgIfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormControlDirective, FormGroup, ValidationErrors, ValidatorFn, AsyncValidatorFn, Validators } from '@angular/forms';
import { combineLatest, Observable, of, pipe, Subject } from 'rxjs';
import { debounceTime, distinct, distinctUntilChanged, distinctUntilKeyChanged, filter, map, tap, withLatestFrom } from 'rxjs/operators';

FormControlDirective
NgIfContext


const censor = (badword = 'batman'): ValidatorFn => (control: AbstractControl): ValidationErrors | null => {
  const hasError = String(control.value).includes(badword)

  // return hasError ? { required: true, minlength: { requiredLength: 15006975 } } : null
  return hasError ? { censor: { badword } } : null
}

const censorAsync: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
  // return this.http.get('/validate?query'+control.value).pipe(map(resp=>resp.error || null))

  return new Observable((observer) => {
    // console.log('Subscribed')
    const hasError = censor('batman')(control)

    const handle = setTimeout(() => {
      // console.log('Next')
      observer.next(hasError)
      // observer.error() // I cannot validate!!
      // console.log('Complete')
      observer.complete()
    }, 1000)

    return () => {
      clearTimeout(handle)
      // console.log('UnSubscribed')
    }
  })
  //.subscribe({next, error, complete}).unsubscribe()
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  advancedOpen = false

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }

  @Input() set query(q: string | null) {
    const field = (this.searchForm.get('query') as FormControl)
    const validator = field.asyncValidator
    field.asyncValidator = null
    // field.patchValue(q, {
    field.setValue(q || '', {
      emitEvent: false,
      onlySelf: true,
      // emitModelToViewChange: false,
      // emitViewToModelChange: false
    })
    field.asyncValidator = validator
    //https://medium.com/ngx/3-ways-to-implement-conditional-validation-of-reactive-forms-c59ed6fc3325
  }

  searchForm = new FormGroup({
    'query': new FormControl('', [
      // Validators.compose([
      // Validators.required,
      Validators.minLength(3),
      // censor('batman'),
      censor('placki')
      // ])!
      // Validators.pattern('/\d*/')
    ], [
      censorAsync
    ]),
    'options': new FormGroup({
      'type': new FormControl('album'),
      'markets': new FormArray([
        new FormGroup({
          'code': new FormControl('PL')
        }),
        new FormGroup({
          'code': new FormControl('GB')
        }),
      ])
    })
  })

  constructor() {
    (window as any).form = this.searchForm
    // this.searchForm.removeControl('query')

    const withValue = <T>(valueChanges: Observable<T>) => pipe(
      filter(status => status === 'VALID'),
      withLatestFrom(valueChanges),
      map(([status, value]) => value),
    )

    const valueChanges = this.searchForm.valueChanges.pipe(
      map((value, counter) => value.query),
      debounceTime(400),
    )
    const statusChanges = this.searchForm.statusChanges

    statusChanges.pipe(
      withValue(valueChanges),
      distinctUntilChanged(),
    )
      .subscribe(this.search)

    //\node_modules\rxjs\internal\operators\map.js

    this.submitClick.subscribe(() => this.submitSearch())
  }

  @Output() search = new EventEmitter<string>();

  ngOnInit(): void {
  }

  private submitClick = new Subject()

  submitSearch() {
    if (this.searchForm.invalid) { return }
    this.search.emit(this.searchForm.value.query)
  }

  // queryField = this.searchForm.get('query') as FormControl
  // typeField = this.searchForm.get('type') as FormControl
  markets = this.searchForm.get(['options', 'markets']) as FormArray

  removeMarket(index: number) {
    this.markets.removeAt(index)
  }

  addMarket() {
    this.markets.push(new FormGroup({
      'code': new FormControl('')
    }))
  }

}
